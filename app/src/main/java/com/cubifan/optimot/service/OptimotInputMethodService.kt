package com.cubifan.optimot.service

import android.inputmethodservice.InputMethodService
import android.inputmethodservice.Keyboard
import android.inputmethodservice.KeyboardView
import android.util.Log
import android.view.KeyEvent
import android.view.View
import com.cubifan.optimot.R

class OptimotInputMethodService : InputMethodService(), KeyboardView.OnKeyboardActionListener {

    private lateinit var keyboardView: KeyboardView
    private lateinit var optimot: Keyboard
    private var currentLayer = Layer.ALPHABET

    enum class Layer {
        ALPHABET, SYMBOLS
    }

    private fun keyboardLayout(layer: Layer) = when (layer) {
        Layer.ALPHABET -> Keyboard(this, R.xml.optimot)
        Layer.SYMBOLS -> Keyboard(this, R.xml.symbols)
    }

    override fun onCreateInputView(): View {
        keyboardView = layoutInflater.inflate(R.layout.keyboard, null) as KeyboardView
        optimot = keyboardLayout(currentLayer)
        return keyboardView.apply {
            keyboard = optimot
            setOnKeyboardActionListener(this@OptimotInputMethodService)
        }
    }

    override fun onPress(key: Int) {
        Log.d("key pressed", "$key")
    }

    override fun onRelease(key: Int) {
        Log.d("key released", "$key")
    }

    override fun onKey(key: Int, keycodes: IntArray?) {
        Log.d("key", "$key, $keycodes")
        val inputConnection = currentInputConnection

        when (key) {
            Keyboard.KEYCODE_DELETE -> inputConnection.deleteSurroundingText(1, 0)
            Keyboard.KEYCODE_DONE -> inputConnection.sendKeyEvent(
                KeyEvent(
                    KeyEvent.ACTION_DOWN,
                    KeyEvent.KEYCODE_ENTER
                )
            )
            else -> inputConnection.commitText(key.toChar().toString(), 1)
        }
    }

    override fun onText(text: CharSequence?) {
        Log.d("key text", "$text")
    }

    private fun changeLayout() {
        optimot = when (currentLayer) {
            Layer.ALPHABET -> {
                currentLayer = Layer.SYMBOLS
                Keyboard(this, R.xml.symbols)
            }
            Layer.SYMBOLS -> {
                currentLayer = Layer.ALPHABET
                Keyboard(this, R.xml.optimot)
            }
        }
        keyboardView.keyboard = optimot
    }

    override fun onKeyLongPress(keyCode: Int, event: KeyEvent?): Boolean {
        return super.onKeyLongPress(keyCode, event)
    }

    override fun swipeLeft() {
        Log.d("swipe", "left")
        changeLayout()
    }

    override fun swipeRight() {
        Log.d("swipe", "right")
        changeLayout()
    }

    override fun swipeDown() {
    }

    override fun swipeUp() {
    }
}